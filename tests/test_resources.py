from mk4p.resources import LabelMatchInfo, PathResource, SimplePattern, StemMatchInfo


def test_simple_pattern_match_string() -> None:
    pattern = SimplePattern("abc%def")
    assert pattern.matches_label("012-789") is None
    match_info = pattern.matches_label("abc-def")
    assert match_info is not None
    assert isinstance(match_info, StemMatchInfo)
    assert match_info.stem == "-"
    assert pattern.matches_label(pattern.spec) == StemMatchInfo(stem="%")


def test_simple_pattern_match_pattern() -> None:
    pattern = SimplePattern("abc%def")
    assert pattern.matches(SimplePattern("012%789")) is None
    assert pattern.matches(SimplePattern("abc%def")) == SimplePattern.MatchInfoX()
    assert pattern.matches(SimplePattern("ab%ef")) == SimplePattern.MatchInfoX()
    assert pattern.matches(SimplePattern("abc_%_def")) is None
    # match_info = pattern.matches(SimplePattern("abc%def"))


def test_simple_pattern_match_path() -> None:
    pattern = SimplePattern("%.o")
    assert pattern.matches(PathResource("main.c")) is None
    assert pattern.matches(PathResource("main.o")) == StemMatchInfo("main")
