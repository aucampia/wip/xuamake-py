import shutil
from pathlib import Path

import pytest

__path__ = Path(__file__)
datadir = __path__.parent / "data"


@pytest.fixture(scope="function")
def c_projec_a_dir(tmp_path: Path) -> Path:
    dest = tmp_path / "c_project_a"
    shutil.copytree(datadir / "c_project_a", dest)
    return dest


@pytest.fixture(scope="function")
def c_projec_b_dir(tmp_path: Path) -> Path:
    dest = tmp_path / "c_project_b"
    shutil.copytree(datadir / "c_project_b", dest)
    return dest


@pytest.fixture(scope="function")
def outputs_dir(tmp_path: Path) -> Path:
    return tmp_path / "outputs"
