import abc
import logging
import subprocess
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, List, Protocol, Set, TypeVar, Union
from unittest.mock import MagicMock

import pytest
from _pytest.capture import CaptureFixture

from mk4p.resources import PathResource, SimplePattern
from mk4p.subprocess import OSSubprocessHandler
from mk4p.utils import PathHelper, chdir, replace_suffix, shq
from mk4p.workflow import RuleABC, RuleFunctionContext, Workflow

# mypy: allow-redefinition


def test_decorator(c_projec_b_dir: Path) -> None:
    stdout_txt = c_projec_b_dir / "stdout.txt"
    prog = c_projec_b_dir / "prog"

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    cfiles = [path.name for path in c_projec_b_dir.glob("*.c")]
    ofiles = replace_suffix(cfiles, ".c", ".o")

    @workflow.rule(
        targets=[*ofiles, prog.name],
        prerequisites=cfiles,
    )
    def rule0(ctx: RuleFunctionContext) -> None:
        cmds = []
        for idx, source in enumerate(ctx.prerequisites):
            cmds.append(f"gcc -c -o {shq(ctx.targets[idx])} {shq(source)}")

        subproc.run(
            f"""
            set -e
            {"; ".join(cmds)}
            gcc -o {ctx.targets[-1]} {shq(ofiles)}
            """,
            shell=True,
            check=True,
        )

    with chdir(c_projec_b_dir):
        workflow.make(prog.name)

    assert prog.exists()

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.returncode == 0
    assert run_result.stderr == ""
    assert run_result.stdout == stdout_txt.read_text()


def test_pattern(c_projec_b_dir: Path) -> None:
    stdout_txt = c_projec_b_dir / "stdout.txt"
    prog = c_projec_b_dir / "prog"

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    cfiles = [path.name for path in c_projec_b_dir.glob("*.c")]
    ofiles = replace_suffix(cfiles, ".c", ".o")

    @workflow.rule(
        targets=[SimplePattern("%.o")],
        prerequisites=[SimplePattern("%.c")],
    )
    def rulec2o(ctx: RuleFunctionContext) -> None:
        subproc.run(
            f"""
            set -e
            gcc -c -o {shq(ctx.targets[0])} {shq(ctx.prerequisites[0])}
            """,
            shell=True,
            check=True,
        )

    @workflow.rule(
        targets=[prog.name],
        prerequisites=ofiles,
    )
    def ruleo2x(ctx: RuleFunctionContext) -> None:
        subproc.run(
            f"""
            set -e
            gcc -o {ctx.targets[0]} {shq(ctx.prerequisites)}
            """,
            shell=True,
            check=True,
        )

    for rule in workflow.rule_respository.rules.values():
        for target_specifier in rule.target_specifiers:
            match_info = target_specifier.matches(PathResource("main.o"))
            logging.debug(
                "target_specifier = %s, match_info = %s", target_specifier, match_info
            )

    match = workflow.rule_respository.match_specifier(PathResource("main.o"))
    assert match is not None

    with chdir(c_projec_b_dir):
        workflow.make(prog.name)

    assert prog.exists()

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.returncode == 0
    assert run_result.stderr == ""
    assert run_result.stdout == stdout_txt.read_text()


def test_chained(c_projec_b_dir: Path) -> None:
    stdout_txt = c_projec_b_dir / "stdout.txt"
    prog = c_projec_b_dir / "prog"

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    cfiles = [path.name for path in c_projec_b_dir.glob("*.c")]
    ofiles = replace_suffix(cfiles, ".c", ".o")

    for ofile, cfile in zip(ofiles, cfiles):

        @workflow.rule(
            targets=[ofile],
            prerequisites=[cfile],
        )
        def rulec2o(ctx: RuleFunctionContext) -> None:
            logging.info("ctx = %s", ctx)
            subproc.run(
                f"""
                set -e
                gcc -c -o {shq(ctx.targets[0])} {shq(ctx.prerequisites[0])}
                """,
                shell=True,
                check=True,
            )

    logging.info("ofiles = %s", ofiles)

    @workflow.rule(
        targets=[prog.name],
        prerequisites=ofiles,
    )
    def ruleo2x(ctx: RuleFunctionContext) -> None:
        logging.info("ctx = %s", ctx)
        logging.info("ctx.prerequisites = %s", ctx.prerequisites)
        subproc.run(
            f"""
            set -e
            gcc -o {ctx.targets[0]} {shq(ctx.prerequisites)}
            """,
            shell=True,
            check=True,
        )

    with chdir(c_projec_b_dir):
        workflow.make(prog.name)

    assert prog.exists()

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.returncode == 0
    assert run_result.stderr == ""
    assert run_result.stdout == stdout_txt.read_text()


def test_missing_prerequisite(tmp_path: Path) -> None:
    prog = tmp_path / "prog"

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    @workflow.rule(
        targets=[prog.name],
        prerequisites=["missing.c"],
    )
    def rule0(ctx: RuleFunctionContext) -> None:
        cmds = []
        for idx, source in enumerate(ctx.prerequisites):
            cmds.append(f"gcc -c -o {shq(ctx.targets[idx])} {shq(source)}")

        subproc.run(
            f"""
            set -e
            {"; ".join(cmds)}
            gcc -o {ctx.targets[-1]} {shq(ctx.prerequisites)}
            """,
            shell=True,
            check=True,
        )

    with chdir(tmp_path):
        with pytest.raises(Exception) as excinfo:
            workflow.make(prog.name)
        assert "missing.c" in str(excinfo)

    assert not prog.exists()


def test_rebuild_on_update(tmp_path: Path) -> None:
    prog = tmp_path / "prog"
    source_c = tmp_path / "source.c"

    source_c.write_text(
        r"""
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    printf("source 0\n");
    return EXIT_SUCCESS;
}
    """
    )

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    call_count = 0

    @workflow.rule(
        targets=[prog.name],
        prerequisites=[source_c.name],
    )
    def ruleo2x(ctx: RuleFunctionContext) -> None:
        nonlocal call_count
        logging.debug("incrementing call_count")
        call_count += 1
        subproc.run(
            f"""
            set -e
            gcc -o {ctx.targets[0]} {shq(ctx.prerequisites)}
            """,
            shell=True,
            check=True,
        )

    with chdir(tmp_path):
        workflow.make(prog.name)
    assert call_count == 1

    assert prog.exists()

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.returncode == 0
    assert run_result.stderr == ""
    assert run_result.stdout == "source 0\n"

    with chdir(tmp_path):
        workflow.make(prog.name)
    assert call_count == 1

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.returncode == 0
    assert run_result.stderr == ""
    assert run_result.stdout == "source 0\n"

    source_c.write_text(
        r"""
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    printf("source 1\n");
    return EXIT_SUCCESS;
}
    """
    )

    with chdir(tmp_path):
        workflow.make(prog.name)
    assert call_count == 2

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.returncode == 0
    assert run_result.stderr == ""
    assert run_result.stdout == "source 1\n"
