import hashlib
import os
from datetime import datetime, timedelta
from pathlib import Path
from typing import Callable, List

from mk4p.utils import FileHelper, StringHelper, replace_suffix


def test_hash_file(tmp_path: Path) -> None:
    text = """\
LW4rOmD7OXeuHdUSkU7cHGAYTjXO8Tfhn7oWzlooOS63n8 JWpC6JCQcNUDrzvZd
wg06cd7 56GsgkMy7Xa QydVaYNnK2mLQItnSmICK2QrFJGeSjKFdEojBzVw48PK
OY0RWmsU8URLbp69f12O4bU7TvAcUn7aYQeXTbDHvz03zbXASuKbBdy3a patYKR
y2gj14J8n4Q8IpF2t8JXqCv2dqJkvCvy9x0DFiiqdbsD9LsQnrzRMdCPl2sWq1wH
TfT 8UG7POXA8yPTJPSGWj6mPe6Hf7WZHpsqRmUXKHcQvjsfpUFRIDYXCpg67i6y
6GCgMbMdJTIL5qDz6Ie4m37zVA904wxRpYpmaOeCbn9 lq08dhKib59334wwjVXP
4bnJLsXschmM2TheRKZdyxvtlvkPuPL2FI1LT3lIjL3tN8arHpaICOZdNoK7L9Ej
GFUZ6lnRyRvXI0XyWe1MgVGUdWdH6VB45kZxBTFrFr6SjEMFbRspUxxa447swe5A
DQd ef4UrNIHMydcCBt1ZMk6aXfE6d2Mtd7iCubt4Zv9EAm2KS0FKlT0ulP1GxnS
pqlW7sQlQhHDVJRLl0QUqfm6iToXav3XxYb6ui8ncQmg1rznX45f9tvFkYgLMmUD
"""
    text_file = tmp_path / "file.txt"
    text_file.write_text(text)
    assert FileHelper.hash_str(text_file) == "5cc25ac3a4b641f7760826c2487898bdf5896c34"


def test_replace_sufixes() -> None:
    var_str: str = replace_suffix("a.c", ".c", ".o")
    assert var_str == "a.o"

    var_list_str: List[str] = replace_suffix(["a.c", "b.c"], ".c", ".o")
    assert var_list_str == ["a.o", "b.o"]

    var_path: Path = replace_suffix(Path("a.c"), ".c", ".o")
    assert var_path == Path("a.o")

    var_list_path: List[Path] = replace_suffix([Path("a.c"), Path("b.c")], ".c", ".o")
    assert var_list_path == [
        Path("a.o"),
        Path("b.o"),
    ]


def test_is_newer(tmp_path: Path) -> None:

    newer_time = datetime.now()
    older_time = newer_time - timedelta(seconds=120)

    older_files: List[Path] = []
    newer_files: List[Path] = []
    for idx in range(3):
        older_file = tmp_path / f"older_file-{idx:03d}"
        older_files.append(older_file)
        older_file.write_text(f"{older_file} content\n")
        os.utime(older_file, (older_time.timestamp(), older_time.timestamp()))

        newer_file = tmp_path / f"newer_file-{idx:03d}"
        newer_files.append(newer_file)
        newer_file.write_text(f"{newer_file} content\n")
        os.utime(newer_file, (newer_time.timestamp(), newer_time.timestamp()))

    assert not FileHelper.is_newer(older_files, newer_files)
    assert not FileHelper.is_newer(older_files[0:1], older_files[-1:])
    assert not FileHelper.is_newer(newer_files[0:1], newer_files[-1:])
    assert FileHelper.is_newer(newer_files, older_files)


def test_prefix_suffix() -> None:
    assert StringHelper.remove_prefix("abc-def", "abc") == "-def"
    assert StringHelper.remove_suffix("abc-def", "def") == "abc-"
    assert StringHelper.remove_enclosing("abc-def", "abc", "def") == "-"

    assert StringHelper.remove_prefix("abc-def", "012") is None
    assert StringHelper.remove_suffix("abc-def", "789") is None
    assert StringHelper.remove_enclosing("abc-def", "012", "789") is None
