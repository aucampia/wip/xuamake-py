#include <stdio.h>
#include <stdlib.h>

#include "a.h"
#include "b.h"

int main(int argc, char *argv[])
{
    a();
    b();
    printf("c_project_b:main.c:main:output\n");
    return EXIT_SUCCESS;
}
