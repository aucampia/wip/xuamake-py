from mk4p.workflow import RuleFunctionContext, Workflow
from mk4p.cli_runner import cli_runner
from mk4p.subprocess import OSSubprocessHandler
from mk4p.utils import shq

subproc = OSSubprocessHandler.facade(True, True, True)
workflow = Workflow()


@workflow.rule(targets=["hello"], prerequisites=["hello.c"])
def prog(ctx: RuleFunctionContext) -> None:
    subproc.run(
        f"""
        set -e
        gcc -o {ctx.targets[0]} {shq(ctx.prerequisites)}
        """,
        shell=True,
        check=True,
    )


@workflow.rule(targets=["run"], prerequisites=["hello"])
def run(ctx: RuleFunctionContext) -> None:
    subproc.run(
        """
        ./hello
        """,
        shell=True,
        check=True,
    )


if __name__ == "__main__":
    cli_runner(workflow)
