import abc
import enum
import hashlib
from dataclasses import dataclass, field
from pathlib import Path
from typing import (
    Callable,
    ClassVar,
    Collection,
    Dict,
    Generic,
    Iterator,
    NamedTuple,
    NewType,
    Protocol,
    Tuple,
    Type,
    TypeVar,
    Union,
)

from attr import Factory
from pytest_subtests import SubTests  # type: ignore[import]

from aucampia.xuamake.subprocess import OSSubprocessHandler, SubprocessFacade
from aucampia.xuamake.utils import hash_file


class Parameters(Protocol):
    pass


ParametersT = TypeVar("ParametersT", bound=Parameters)


class ParametersABC(Parameters, abc.ABC):
    pass


@dataclass
class BlankParameters(ParametersABC):
    pass


class ExecutionContext(Protocol):
    pass


class ExecutionContextABC(ExecutionContext, abc.ABC):
    pass


@dataclass
class BlankContext(ExecutionContextABC):
    pass


@dataclass
class DefaultContext(ExecutionContextABC):
    subprocess: SubprocessFacade = field(
        default_factory=lambda: OSSubprocessHandler.facade(True)
    )


class Paramterized(Protocol):
    @classmethod
    def parameters_type(cls) -> Type[Parameters]:
        ...

    @property
    def parameters(self) -> Parameters:
        ...


class ParamterizedABC(Paramterized, abc.ABC):
    @classmethod
    @abc.abstractmethod
    def parameters_type(cls) -> Type[Parameters]:
        ...

    @property
    @abc.abstractmethod
    def parameters(self) -> Parameters:
        ...


class Contextualized(Protocol):
    @classmethod
    def context_type(cls) -> Type[ExecutionContext]:
        ...

    @property
    def context(self) -> Parameters:
        ...


class ContextualizedABC(Contextualized):
    @classmethod
    @abc.abstractmethod
    def context_type(cls) -> Type[ExecutionContext]:
        ...

    @property
    @abc.abstractmethod
    def context(self) -> Parameters:
        ...


class Resource(Contextualized, Paramterized, Protocol):
    def __init__(self, context: ExecutionContext, parameters: Parameters):
        ...

    @property
    def hash(self) -> str:
        ...

    @property
    def exists(self) -> bool:
        ...


class ResourceABC(Resource, ContextualizedABC, ParamterizedABC, abc.ABC):
    @abc.abstractmethod
    def __init__(self, context: ExecutionContext, parameters: Parameters):
        ...

    @property
    @abc.abstractmethod
    def hash(self) -> str:
        ...

    @property
    @abc.abstractmethod
    def exists(self) -> bool:
        ...


class NamedResource(NamedTuple):
    name: str
    resource: Resource


class Cardinatlity(enum.Enum):
    ONE = "one"
    MANY = "many"


CardinatlitySpec = Union[int, Cardinatlity]


class ResourceTypeSpec(NamedTuple):
    resource_type: Type[Resource]
    cardinatlity: CardinatlitySpec


ResourceSpec = Union[Resource, Collection[Resource], ResourceTypeSpec, Type[Resource]]
Dependency = Union[ResourceSpec, "Task"]


class NamedDependency(NamedTuple):
    name: str
    resource: ResourceSpec


class Task(Contextualized, Paramterized, Protocol):
    def __init__(self, context: ExecutionContext, parameters: Parameters):
        ...

    @classmethod
    def requires(cls) -> Iterator[NamedDependency]:
        ...

    @classmethod
    def outputs(cls) -> Iterator[NamedResource]:
        ...

    @property
    def current(self) -> bool:
        ...


class TaskABC(Task, ContextualizedABC, ParamterizedABC, abc.ABC):
    @abc.abstractmethod
    def __init__(self, context: ExecutionContext, parameters: Parameters):
        ...

    @property
    @abc.abstractmethod
    def current(cls) -> bool:
        ...


class BaseTask(TaskABC):
    pass


class ResourceFactory(Protocol):
    def __init__(self, context: ExecutionContext, parameters: Parameters):
        ...

    def make_resource(
        self, resource_type: Type[Resource], parameters: Parameters
    ) -> Resource:
        ...


class ResourceFactoryABC(ResourceFactory, ContextualizedABC, ParamterizedABC, abc.ABC):
    @abc.abstractmethod
    def make_resource(
        self, resource_type: Type[Resource], parameters: Parameters
    ) -> Resource:
        ...


ResourceT = TypeVar("ResourceT", bound=Resource)


class Scope(ResourceFactoryABC):
    factories: Collection[ResourceFactory]
    tasks: Collection[Task]

    def make_resource(
        self, resource_type: Type[Resource], parameters: Parameters
    ) -> Resource:
        ...


def test_cxx(subtests: SubTests) -> None:
    @dataclass
    class FileParameters(ParametersABC):
        path: Path

    @dataclass
    class File(ResourceABC):
        context: DefaultContext
        parameters: FileParameters

        @classmethod
        def context_type(cls) -> Type[ExecutionContext]:
            return DefaultContext

        @classmethod
        def parameters_type(cls) -> Type[FileParameters]:
            return FileParameters

        @property
        def hash(self) -> str:
            return hash_file(self.parameters.path)

        @property
        def exists(self) -> bool:
            return self.parameters.path.exists()

    @dataclass
    class FileFactory(ResourceFactoryABC):
        context: DefaultContext
        parameters: BlankParameters

        @classmethod
        def context_type(cls) -> Type[ExecutionContext]:
            return DefaultContext

        @classmethod
        def parameters_type(cls) -> Type[ExecutionContext]:
            return BlankParameters

        def make_resource(
            self, resource_type: Type[Resource], parameters: Parameters
        ) -> Resource:
            return resource_type(self.context, parameters)

    class ExecutableFile(File):
        pass

    # main_exe = ResourceSpec(ExecutableFile, parameters="")

    # workflow.make(main_exe)
