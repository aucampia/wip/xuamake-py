from dataclasses import dataclass
from pathlib import Path
from typing import Collection, Generic, Protocol, Type, TypeVar

# from pytest_subtests import SubTests  # type: ignore[import]

# ParametersT = TypeVar("ParametersT")


# class Resource(Protocol):
#     @classmethod
#     @property
#     def name(self) -> str:
#         ...


# class Rule(Protocol, Generic[ParametersT]):
#     @classmethod
#     @property
#     def name(self) -> str:
#         ...

#     @classmethod
#     def for_parameters(parameters: ParametersT) -> "Rule[ParametersT]":
#         ...

#     @classmethod
#     def class_outputs(parameters: ParametersT) -> Collection[Type[Resource]]:
#         ...
