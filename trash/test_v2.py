from typing import Any, Callable, Protocol, Type, TypeVar


class FileResource:
    pass


class Task:
    pass


CallableAnyT = Callable[..., Any]
CallableTV = TypeVar("CallableTV", bound=CallableAnyT)


def task() -> Callable[[CallableTV], Type[Task]]:
    def inner(function: CallableTV) -> Type[Task]:
        return Task

    return inner


class Context:
    pass


def file_task(input: Any, output: Any) -> Callable[[CallableTV], Type[Task]]:
    def inner(function: CallableTV) -> Type[Task]:
        return Task

    return inner


class FileInput:
    pass


class FileOutput:
    pass


################################################################################
# code
################################################################################


@file_task(input="{}.asciidoc", output="{}.html")
def adoc2html(ctx: Context, input: Any, output: Any) -> None:
    f"""
    asciidoctor
        --trace -r asciidoctor-diagram
        --backend html5
        --attribute git-version="43620f3"
        --attribute git-date="2020-11-19"
        --attribute git-datetime="2020-11-19 21:21:30 +0100"
        --attribute git-subject="checkpoint"
        --attribute git-commit-count="21"
        --attribute git-local-changes="M"
        --attribute date-today="2021-07-10"
        --attribute year-today="2021"
        --attribute datetime-now="2021-07-10 13:20:42"
        --attribute source-highlighter="pygments"
        --attribute imagesoutdir="/home/iwana/syncthing/sw-wpw/d/gitlab.com/aucampia/templates/asciidoctor/build/images"
        --attribute imagesdir="/home/iwana/syncthing/sw-wpw/d/gitlab.com/aucampia/templates/asciidoctor/build/images"
        --attribute data-uri
        --attribute png_or_svg=svg
        --attribute imagegen_ext=svg
        --attribute link_suffix=.html
        --out-file {output}
    """
