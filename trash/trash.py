def fake() -> None:
    pass
    # class CXXObjFactory(TaskABC):
    #     @dataclass
    #     class Parameters(ParametersABC):
    #         input_file: Path
    #         output_file: Path

    #     context: DefaultContext
    #     parameters: Parameters

    #     @classmethod
    #     def context_type(cls) -> Type[ExecutionContext]:
    #         return DefaultContext

    #     @classmethod
    #     def parameters_type(cls) -> Type[ExecutionContext]:
    #         return Parameters

    #     @property
    #     def current(cls) -> bool:
    #         ...

    # @dataclass
    # class DockerImage(ResourceABC):
    #     context: DefaultContext
    #     registry: str
    #     repo: str
    #     tag: str

    #     @classmethod
    #     def context_type(cls) -> Type[ExecutionContext]:
    #         return DefaultContext

    #     @property
    #     def reference(self) -> str:
    #         return f"{self.registry}/{self.repo}:{self.tag}"

    #     @property
    #     def exists(self) -> bool:
    #         self.context.subprocess.run(
    #             "docker",
    #             "image",
    #             "ls",
    #         )

    # class BuildDockerImage(TaskABC):
    #     @dataclass
    #     class Parameters:
    #         input_file: Path
    #         output_file: Path

    #     def requires(self) -> None:
    #         return

    #     @classmethod
    #     def outputs(self) -> None:
    #         ...
