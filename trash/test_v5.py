import inspect
import os
import re
import shlex
from contextlib import contextmanager
from pathlib import Path
from typing import (
    Any,
    Callable,
    ClassVar,
    Dict,
    Iterator,
    List,
    Optional,
    Pattern,
    Set,
    TypeVar,
    cast,
    get_type_hints,
)
from dataclasses import dataclass

"""

class FileResource:
    pass







class InputPath(Path):
    pass


class OutputPath(Path):
    pass


class ArgSpec:
    pass


class ArgBinding:
    def __init__(self, spec: ArgSpec) -> None:
        self.__spec = spec


class InputFile(ArgSpec):
    def __init__(self, pattern: str) -> None:
        self.pattern = re.compile(pattern)

    @classmethod
    def input(cls, name: str) -> InputPath:
        spec = cls(name)
        return cast(InputPath, ArgBinding(spec))

    @classmethod
    def input_pattern(cls, name: str) -> InputPath:
        spec = cls(name)
        return cast(InputPath, ArgBinding(spec))

    @classmethod
    def output(cls, name: str) -> OutputPath:
        spec = cls(name)
        return cast(OutputPath, ArgBinding(spec))

    @classmethod
    def output_pattern(cls, name: str) -> InputPath:
        spec = cls(name)
        return cast(InputPath, ArgBinding(spec))


class ParamMeta:
    empty = inspect.Parameter.empty

    def __init__(
        self,
        *,
        name: str,
        default: Any = inspect.Parameter.empty,
        annotation: Any = inspect.Parameter.empty,
    ) -> None:
        self.name = name
        self.default = default
        self.annotation = annotation


def get_params_from_function(func: Callable[..., Any]) -> Dict[str, ParamMeta]:
    signature = inspect.signature(func)
    type_hints = get_type_hints(func)
    params = {}
    for param in signature.parameters.values():
        annotation = param.annotation
        if param.name in type_hints:
            annotation = type_hints[param.name]
        params[param.name] = ParamMeta(
            name=param.name, default=param.default, annotation=annotation
        )
    return params



"""


# @contextmanager
# def chdir(new_wd: Path) -> Iterator[Path]:
#     old_wd = os.getcwd()
#     os.chdir(new_wd)
#     try:
#         yield new_wd
#     finally:
#         os.chdir(old_wd)


# def sq(input: Any) -> str:
#     return shlex.quote(f"{input}")


# CallableAnyT = Callable[..., Any]
# CallableTV = TypeVar("CallableTV", bound=CallableAnyT)


# class Context:
#     pass


# class Rule:
#     def run(self, context: Context) -> None:
#         pass


# class FunctionRule(Rule):
#     def __init__(self, function: CallableTV) -> None:
#         super().__init__()

#     def run(self, context: Context) -> None:
#         pass


# @dataclass
# class FilePattern:
#     prefix: str
#     stem: str
#     suffix: str

#     _re: ClassVar[Pattern[str]] = re.compile(
#         r"^(?P<prefix>[^<>]+)*(?P<stem><[^>]+>)(?P<suffix>[^<>]+)*$"
#     )

#     # def __init__(self, spec: str, valid_stems: Optional[Set[str]] = None) -> None:
#     #     if valid_stems is None:
#     #         valid_stems = set()

#     #     match = self._re.match(spec)
#     #     if not match:
#     #         raise ValueError(f"spec {spec!r} did not match regex {self._re}")

#     #     self.prefix = match.group("prefix")
#     #     self.stem = match.group("stem")
#     #     self.suffix = match.group("suffix")

#     @property
#     def spec(self) -> str:
#         return f"{self.prefix}<{self.stem}>{self.suffix}"

#     @classmethod
#     def parse(cls) -> str:
#         pass


# def test_filepattern() -> None:
#     FilePattern("a<stem>")


# class TargetSpec:
#     pass


# class TargetName(TargetSpec):
#     def __init__(self, name: str) -> None:
#         super().__init__()
#         self.name = name


# class TargetPattern(TargetSpec):
#     def __init__(self, pattern: str) -> None:
#         super().__init__()
#         self.pattern = pattern


# class PrereqisiteSpec:
#     pass


# class PrereqisiteName(TargetSpec):
#     pass


# class PrereqisitePattern(TargetSpec):
#     pass


# class Maker:
#     rules = List[Rule]

#     def __init__(self) -> None:
#         pass

#     def run(self) -> None:
#         pass

#     def rule(
#         self, targets=Set[TargetSpec], prerequisites=Set[TargetSpec]
#     ) -> Callable[[CallableTV], Rule]:
#         def inner(function: CallableTV) -> Rule:

#             return FunctionRule(function)

#         return inner


# def test_maker(tmp_path: Path) -> None:

#     m = Maker()

#     @m.rule(
#         targets={TargetPattern("output/<stem>.html")},
#         prerequisites={PrereqisitePattern("<name>.adoc")},
#     )
#     def adoc2html(
#         ctx: Context,
#         targets: Path,
#         output_file: OutputPath = FilePattern.output("output/{{name}}.html"),
#     ) -> None:
#         f"""
#         asciidoctor \\
#             --trace \\
#             --backend html5 \\
#             --out-file {sq(output_file)} \\
#             {sq(input_file)}
#         """

#     (tmp_path / "input.adoc").write_text(
#         """\
# = title


# == S1

# === S2


# ==== S3
# """
#     )

#     with chdir(tmp_path):
#         assert os.getcwd() == f"{tmp_path}"
#         m.run()
