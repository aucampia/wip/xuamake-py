import abc
import subprocess
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, List, Protocol, Set, TypeVar, Union

from _pytest.capture import CaptureFixture

from aucampia.xuamake.make import RuleABC, RuleRunContext, Workflow
from aucampia.xuamake.subprocess import OSSubprocessHandler
from aucampia.xuamake.utils import PathHelper, chdir, replace_suffix, shq

# mypy: allow-redefinition


def test_basic(tmp_path: Path, capfd: CaptureFixture[str]) -> None:
    subproc = OSSubprocessHandler.facade(True, True, True)

    source_path = tmp_path / "helloworld.c"

    source_path.write_text(
        r"""\
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   printf("hello world\n");
   return EXIT_SUCCESS;
}
"""
    )
    target_path = tmp_path / "helloworld"

    class Eg1(RuleABC):
        targets = [target_path.name]
        prerequisites = [source_path.name]

        def run(self) -> None:
            subproc.run(
                f"""\
            set -ex
            gcc -o {self.targets[0]} {shq(self.prerequisites)}
            """,
                shell=True,
                check=True,
            )

    workflow = Workflow()
    workflow.add_rule(Eg1())

    assert not target_path.exists()

    with chdir(tmp_path):
        workflow.make(target_path.name)

    assert target_path.exists()

    run_result = subprocess.run(
        PathHelper.to_str(target_path),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.stderr == ""
    assert run_result.stdout == "hello world\n"


def test_decorator(tmp_path: Path) -> None:

    source_a_c = tmp_path / "source_a.c"
    source_a_c.write_text(
        r"""\
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   printf("hello world\n");
   return EXIT_SUCCESS;
}
"""
    )

    source_a_o = tmp_path / "source_a.o"

    source_b_c = tmp_path / "source_b.c"
    source_b_c.write_text(
        r"""\
void nop(void) {}
"""
    )
    source_b_o = tmp_path / "source_b.o"

    prog = tmp_path / "prog"

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    @workflow.rule(
        targets=[source_a_o.name, source_b_o.name, prog.name],
        prerequisites=[source_a_c.name, source_b_c.name],
    )
    def make_prog(ctx: RuleRunContext) -> None:
        subproc.run(
            f"""\
            set -ex
            gcc -c -o {ctx.targets[0]} {shq(ctx.prerequisites[0])}
            gcc -c -o {ctx.targets[1]} {shq(ctx.prerequisites[1])}
            gcc -o {ctx.targets[-1]} {ctx.targets[0]} {ctx.targets[1]}
            """,
            shell=True,
            check=True,
        )

    with chdir(tmp_path):
        workflow.make(prog.name)

    assert prog.exists()

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.stderr == ""
    assert run_result.stdout == "hello world\n"


def test_decorator_b(c_project_dir: Path) -> None:
    prog = c_project_dir / "prog"

    workflow = Workflow()

    subproc = OSSubprocessHandler.facade(True, True, True)

    cfiles = [path.name for path in c_project_dir.glob("*.c")]
    ofiles = replace_suffix(cfiles, ".c", ".o")

    @workflow.rule(
        targets=[*ofiles, prog.name],
        prerequisites=cfiles,
    )
    def rule0(ctx: RuleRunContext) -> None:

        cmds = []
        for idx, source in enumerate(ctx.prerequisites):
            cmds.append(f"gcc -c -o {shq(ctx.targets[idx])} {shq(source)}")

        subproc.run(
            f"""
            set -e
            {"; ".join(cmds)}
            gcc -o {ctx.targets[-1]} {shq(ofiles)}
            """,
            shell=True,
            check=True,
        )

    with chdir(c_project_dir):
        workflow.make(prog.name)

    assert prog.exists()

    run_result = subprocess.run(
        PathHelper.to_str(prog),
        check=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        encoding=sys.getdefaultencoding(),
    )
    assert run_result.stderr == ""
    assert run_result.stdout == "hello world\n"
