import inspect
import os
import re
import shlex
from contextlib import contextmanager
from pathlib import Path
from typing import (
    Any,
    Callable,
    Dict,
    Iterator,
    List,
    Pattern,
    TypeVar,
    cast,
    get_type_hints,
)


class FileResource:
    pass


class Context:
    pass


class Task:
    def run(self, context: Context) -> None:
        pass


CallableAnyT = Callable[..., Any]
CallableTV = TypeVar("CallableTV", bound=CallableAnyT)


class FunctionTask(Task):
    def __init__(self, function: CallableTV) -> None:
        super().__init__()

    def run(self, context: Context) -> None:
        pass


class Maker:
    tasks = List[Task]

    def __init__(self) -> None:
        pass

    def run(self) -> None:
        pass

    def task(self) -> Callable[[CallableTV], Task]:
        def inner(function: CallableTV) -> Task:

            return FunctionTask(function)

        return inner


def sq(input: Any) -> str:
    return shlex.quote(f"{input}")


class InputPath(Path):
    pass


class OutputPath(Path):
    pass


class ArgSpec:
    pass


class ArgBinding:
    def __init__(self, spec: ArgSpec) -> None:
        self.__spec = spec


class FilePattern(ArgSpec):
    def __init__(self, pattern: str) -> None:
        self.pattern = re.compile(pattern)

    @classmethod
    def bind(cls, pattern: str) -> InputPath:
        spec = cls(pattern)
        return cast(InputPath, ArgBinding(spec))


class DerivedOutfile(ArgSpec):
    def __init__(self, template: str) -> None:
        self.template = template

    @classmethod
    def bind(cls, pattern: str) -> OutputPath:
        spec = cls(pattern)
        return cast(OutputPath, ArgBinding(spec))


class ParamMeta:
    empty = inspect.Parameter.empty

    def __init__(
        self,
        *,
        name: str,
        default: Any = inspect.Parameter.empty,
        annotation: Any = inspect.Parameter.empty,
    ) -> None:
        self.name = name
        self.default = default
        self.annotation = annotation


def get_params_from_function(func: Callable[..., Any]) -> Dict[str, ParamMeta]:
    signature = inspect.signature(func)
    type_hints = get_type_hints(func)
    params = {}
    for param in signature.parameters.values():
        annotation = param.annotation
        if param.name in type_hints:
            annotation = type_hints[param.name]
        params[param.name] = ParamMeta(
            name=param.name, default=param.default, annotation=annotation
        )
    return params


@contextmanager
def chdir(new_wd: Path) -> Iterator[Path]:
    old_wd = os.getcwd()
    os.chdir(new_wd)
    try:
        yield new_wd
    finally:
        os.chdir(old_wd)


def test_maker(tmp_path: Path) -> None:

    m = Maker()

    @m.task()
    def adoc2html(
        ctx: Context,
        input_file: InputPath = FilePattern.bind(r"(?P<stem>[^/]*)[.]adoc"),
        output_file: OutputPath = DerivedOutfile.bind("output/{{stem}}.html"),
    ) -> None:
        f"""
        asciidoctor \\
            --trace \\
            --backend html5 \\
            --out-file {sq(output_file)} \\
            {sq(input_file)}
        """

    (tmp_path / "input.adoc").write_text(
        """\
= title


== S1

=== S2


==== S3
"""
    )

    with chdir(tmp_path):
        assert os.getcwd() == f"{tmp_path}"
        m.run()
