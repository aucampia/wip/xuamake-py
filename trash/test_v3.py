import logging
from pathlib import Path
from typing import Any, Callable, Generic, List, Protocol, Type, TypeVar


class FileResource:
    pass


class Context:
    pass


class Task:
    def run(self, context: Context) -> None:
        pass


CallableAnyT = Callable[..., Any]
CallableTV = TypeVar("CallableTV", bound=CallableAnyT)


def glob_file(pattern: str) -> Path:
    pass


def derived_file(template: str) -> Path:
    pass


class FunctionTask(Task):
    def __init__(self, function: CallableTV) -> None:
        super().__init__()

    def run(self, context: Context) -> None:
        pass


class Maker:
    tasks = List[Task]

    def __init__(self) -> None:
        pass

    def run(self) -> None:
        pass

    def task(self) -> Callable[[CallableTV], Task]:
        def inner(function: CallableTV) -> Task:

            return FunctionTask(function)

        return inner


GenericT = TypeVar("GenericT")


class Input(Generic[GenericT]):
    pass


class Output(Generic[GenericT]):
    pass


m = Maker()


@m.task()
def adoc2html(
    ctx: Context,
    input_file: Path,
    output_file: Path,
) -> None:
    logging.info("building %s from %s", input_file, output_file)


class Branch:
    def step(self, task: Task) -> "Branch":
        return Branch()


class Pipeline:
    def step(self, task: Task) -> Branch:
        return Branch()


def test_processing() -> None:
    m.run()

    # pipeline = Pipeline()
    # pipeline.step(glob("*.asciidoc")).steps()
