import collections.abc
import hashlib
import os
import shlex
from contextlib import contextmanager
from os import PathLike
from pathlib import Path, PurePath
from typing import (
    Any,
    Callable,
    Collection,
    Iterable,
    Iterator,
    List,
    Optional,
    Sequence,
    Tuple,
    TypeVar,
    Union,
    overload,
)

HashAlgoT = Callable[[], "hashlib._Hash"]


# def hash_file(file: Path, algo: HashAlgoT = hashlib.sha1) -> str:
#     hash = algo()
#     with file.open("rb") as fp:
#         for chunk in iter(lambda: fp.read(1024 * 4), b""):
#             hash.update(chunk)
#     return hash.hexdigest()


Pathlikeish = Union[str, PathLike]

Pathish = Union[str, PurePath]
PathishT = TypeVar("PathishT", bound=Pathish)


def pathish_to_str(path: Pathlikeish) -> str:
    return f"{path}"


def pathish_to_path(path: Pathlikeish) -> Path:
    if isinstance(path, Path):
        return path
    return Path(path)


class PathHelper:
    @overload
    @classmethod
    def to_str(cls, input: Pathlikeish) -> str:
        ...

    @overload
    @classmethod
    def to_str(cls, input: List[Pathlikeish]) -> List[str]:
        ...

    @classmethod
    def to_str(
        cls, input: Union[Pathlikeish, List[Pathlikeish]]
    ) -> Union[str, List[str]]:
        if isinstance(input, PathLike):
            return f"{input}"
        else:
            return [f"{item}" for item in input]

    @classmethod
    def create(cls, path: Pathlikeish) -> Path:
        if isinstance(path, Path):
            return path
        return Path(path)


@contextmanager
def chdir(new_wd: Path) -> Iterator[Path]:
    old_wd = os.getcwd()
    os.chdir(new_wd)
    try:
        yield new_wd
    finally:
        os.chdir(old_wd)


GenericT = TypeVar("GenericT")


class IterableHelper:
    @classmethod
    def gte(cls, lhs: Iterable[float], rhs: Iterable[float]) -> bool:
        # lhs >= rhs
        min_lhs = min(lhs, default=None)
        max_rhs = max(rhs, default=None)
        if min_lhs is None:
            return False
        if max_rhs is None:
            return True
        return min_lhs > max_rhs


class FileHelper:
    @classmethod
    def hash_str(cls, file: Pathlikeish, algo: HashAlgoT = hashlib.sha1) -> str:
        hash = algo()
        file_path = PathHelper.create(file)
        with file_path.open("rb") as fp:
            for chunk in iter(lambda: fp.read(1024 * 4), b""):
                hash.update(chunk)
        return hash.hexdigest()

    @classmethod
    def mtime_ns(cls, path: Pathlikeish) -> Optional[int]:
        path = pathish_to_path(path)
        if not path.exists():
            return None
        return path.stat().st_mtime_ns

    @classmethod
    def mtime(cls, path: Pathlikeish) -> Optional[float]:
        path = pathish_to_path(path)
        if not path.exists():
            return None
        return path.stat().st_mtime

    @classmethod
    def exist(cls, paths: Iterable[Pathlikeish]) -> bool:
        for path in paths:
            if not pathish_to_path(path).exists():
                return False
        return True

    @classmethod
    def is_newer(
        cls, newer_files: Iterable[Pathlikeish], older_files: Iterable[Pathlikeish]
    ) -> bool:
        # return IterableHelper.gte(
        #     [
        #         pathish_to_path(newer_file).stat().st_mtime_ns
        #         for newer_file in newer_files
        #     ],
        #     [
        #         pathish_to_path(older_files).stat().st_mtime_ns
        #         for older_files in older_files
        #     ],
        # )
        largest_older_mtime: Optional[float] = None
        for older_file in older_files:
            older_mtime = pathish_to_path(older_file).stat().st_mtime
            if largest_older_mtime is None or older_mtime > largest_older_mtime:
                largest_older_mtime = older_mtime
        smallest_newer_mtime: Optional[float] = None
        for newer_file in newer_files:
            newer_file_path = pathish_to_path(newer_file)
            if not newer_file_path.exists():
                smallest_newer_mtime = None
                break
            newer_mtime = newer_file_path.stat().st_mtime
            if smallest_newer_mtime is None or newer_mtime < smallest_newer_mtime:
                smallest_newer_mtime = newer_mtime
        if smallest_newer_mtime is None:
            return False
        if largest_older_mtime is None:
            return True
        return smallest_newer_mtime > largest_older_mtime


def shq(input: Any) -> str:
    if (
        isinstance(input, str)
        or isinstance(input, PathLike)
        or isinstance(input, PurePath)
    ):
        return shlex.quote(f"{input}")
    if isinstance(input, collections.abc.Iterable):
        return " ".join(shlex.quote(f"{arg}") for arg in input)
    return shlex.quote(f"{input}")


def _replace_suffix(input: PathishT, suffix: str, replacement: str) -> PathishT:
    input_str = f"{input}"
    if input_str.endswith(suffix):
        cls = type(input)
        return cls(input_str[: -len(suffix)] + replacement)  # type: ignore[return-value]
    return input


Replaceable = Union[Pathish, Iterable[Pathish]]
ReplaceableT = TypeVar("ReplaceableT", bound=Replaceable)


def replace_suffix(input: ReplaceableT, suffix: str, replacement: str) -> ReplaceableT:
    if isinstance(input, str) or isinstance(input, PurePath):
        return _replace_suffix(input, suffix, replacement)  # type: ignore[return-value]
    return [_replace_suffix(item, suffix, replacement) for item in input]  # type: ignore[return-value]


class StringHelper:
    @classmethod
    def remove_prefix(cls, value: str, prefix: str) -> Optional[str]:
        if not value.startswith(prefix):
            return None
        return value[len(prefix) :]

    @classmethod
    def remove_suffix(cls, value: str, suffix: str) -> Optional[str]:
        if not value.endswith(suffix):
            return None
        return value[: -len(suffix)]

    @classmethod
    def remove_enclosing(cls, value: str, prefix: str, suffix: str) -> Optional[str]:
        remainder = cls.remove_prefix(value, prefix)
        if remainder is None:
            return None
        remainder = cls.remove_suffix(remainder, suffix)
        return remainder
