import abc
import os
import os.path
from dataclasses import dataclass
from typing import Callable, Iterable, Iterator, List, Optional, Protocol, Union

from typing_extensions import runtime_checkable

from .utils import FileHelper, Pathlikeish, StringHelper, pathish_to_path


@dataclass(frozen=True)
class MatchInfo:
    pass


@dataclass(frozen=True)
class LabelMatchInfo(MatchInfo):
    pass


@dataclass(frozen=True)
class LabelExactMatchInfo(LabelMatchInfo):
    pass


@dataclass(frozen=True)
class StemMatchInfo(LabelMatchInfo):
    stem: str


@runtime_checkable
class ResourceSpecifier(Protocol):
    def matches_label(self, name: str) -> Optional[LabelMatchInfo]:
        ...

    def matches(self, other: "ResourceSpecifier") -> Optional[MatchInfo]:
        ...


class ResourceSpecifierABC(abc.ABC):
    @abc.abstractmethod
    def matches_label(self, name: str) -> Optional[LabelMatchInfo]:
        ...

    @abc.abstractmethod
    def matches(self, other: "ResourceSpecifier") -> Optional[MatchInfo]:
        ...


@runtime_checkable
class ExactResourceSpecifier(ResourceSpecifier, Protocol):
    def exists(self) -> bool:
        ...

    # def mtime_ns(self) -> Optional[int]:
    #     ...

    @property
    def label(self) -> str:
        ...


class ExactResourceSpecifierABC(ResourceSpecifierABC, abc.ABC):
    @abc.abstractmethod
    def exists(self) -> bool:
        ...

    # @abc.abstractmethod
    # def mtime_ns(self) -> Optional[int]:
    #     ...

    @property
    @abc.abstractmethod
    def label(self) -> str:
        ...


@runtime_checkable
class StemResourceSpecifier(ResourceSpecifier, Protocol):

    # def exists(self, stem: str) -> bool:
    #     ...

    # def mtime(self, stem: str) -> Optional[int]:
    #     ...

    def stem_to_label(self, stem: str) -> str:
        ...

    def bind_stem(self, stem: str) -> ExactResourceSpecifier:
        ...


# @runtime_checkable
# class LabeledResource(ResourceSpecifier, Protocol):
#     @property
#     def label(self) -> str:
#         ...


@dataclass
class PathResource(ExactResourceSpecifierABC):
    path: str

    def __post_init__(self) -> None:
        self.path = os.path.normpath(self.path)

    def matches(self, other: "ResourceSpecifier") -> Optional[MatchInfo]:
        if isinstance(other, type(self)):
            return self.matches_label(other.path)
        return None

    def matches_label(self, name: str) -> Optional[LabelExactMatchInfo]:
        if self.path == os.path.normpath(name):
            return LabelExactMatchInfo()
        return None

    def exists(self) -> bool:
        return os.path.exists(self.path)

    def mtime_ns(self) -> Optional[int]:
        if not self.exists():
            return None
        return os.stat(self.path).st_mtime_ns

    @property
    def label(self) -> str:
        return self.path


@dataclass
class PhonyResource(ExactResourceSpecifierABC):
    name: str

    def matches(self, other: "ResourceSpecifier") -> Optional[MatchInfo]:
        if isinstance(other, type(self)):
            return self.matches_label(other.label)
        return None

    def matches_label(self, name: str) -> Optional[LabelExactMatchInfo]:
        if self.name == name:
            return LabelExactMatchInfo()
        return None

    def exists(self) -> bool:
        return False

    def mtime_ns(self) -> Optional[int]:
        return None

    @property
    def label(self) -> str:
        return self.name


ResourceSpecifierish = Union[ResourceSpecifier, str]


@dataclass
class SimplePattern(StemResourceSpecifier):
    # @dataclass
    # class MatchInfo:
    #     pass

    # @dataclass
    # class StringMatch(MatchInfo):
    #     stem: str

    @dataclass
    class MatchInfoX(MatchInfo):
        pass

    def __init__(self, spec: str) -> None:
        self.spec = spec
        self.prefix, self.suffix = spec.split("%", 1)

    # def matches(
    #     self, value: Union[Pathlikeish, "SimplePattern"]
    # ) -> Optional["MatchInfo"]:
    #     """ """
    #     if isinstance(value, SimplePattern):
    #         return self.matches_pattern(value)
    #     else:
    #         return self.matches_name(f"{value}")

    def matches(self, other: "ResourceSpecifier") -> Optional[MatchInfo]:
        if isinstance(other, type(self)):
            return self.matches_pattern(other)
        if isinstance(other, ExactResourceSpecifier):
            return self.matches_label(other.label)
        return None

    def matches_pattern(
        self, value: "SimplePattern"
    ) -> Optional["SimplePattern.MatchInfoX"]:
        if not self.prefix.startswith(value.prefix):
            return None
        if not self.suffix.endswith(value.suffix):
            return None
        return self.MatchInfoX()

    def matches_label(self, value: str) -> Optional[StemMatchInfo]:
        stem = StringHelper.remove_enclosing(value, self.prefix, self.suffix)
        if stem is None:
            return None
        return StemMatchInfo(stem=stem)

    def stem_to_label(self, stem: str) -> str:
        return f"{self.prefix}{stem}{self.suffix}"

    def bind_stem(self, stem: str) -> ExactResourceSpecifier:
        return PathResource(self.stem_to_label(stem))

    def __repr__(self) -> str:
        return f"{self.__module__}.{self.__class__.__name__}(spec={self.spec!r})"


ResourceSpecifierConstructor = Callable[[str], ResourceSpecifier]


class ResourceSpecifierHelper:
    @classmethod
    def matches_label(
        cls, spec: Union[str, ResourceSpecifier], label: Pathlikeish
    ) -> Optional[MatchInfo]:
        if isinstance(spec, str):
            if spec == label:
                return LabelExactMatchInfo()
            return None
        return spec.matches_label(f"{label}")

    @classmethod
    def as_specifiers(
        cls,
        specifiers: Iterable[Union[ResourceSpecifier, str]],
        default_factory: ResourceSpecifierConstructor = PathResource,
    ) -> List[ResourceSpecifier]:
        result: List[ResourceSpecifier] = []
        for specifier in specifiers:
            if isinstance(specifier, str):
                result.append(default_factory(specifier))
            else:
                result.append(specifier)
        return result

    @classmethod
    def as_specifier(
        cls,
        specifiers: Iterable[Union[ResourceSpecifier, str]],
        default_factory: ResourceSpecifierConstructor = PathResource,
    ) -> List[ResourceSpecifier]:
        result: List[ResourceSpecifier] = []
        for specifier in specifiers:
            if isinstance(specifier, str):
                result.append(default_factory(specifier))
            else:
                result.append(specifier)
        return result

    @classmethod
    def bind_resources(
        cls, specifiers: Iterable[ResourceSpecifier], stem: Optional[str]
    ) -> List[ExactResourceSpecifier]:
        result: List[ExactResourceSpecifier] = []
        for specifier in specifiers:
            if isinstance(specifier, ExactResourceSpecifier):
                result.append(specifier)
            elif isinstance(specifier, StemResourceSpecifier):
                if stem is None:
                    raise ValueError(f"stem is None, cannot bind {specifier}")
                result.append(specifier.bind_stem(stem))
            else:
                raise ValueError(f"Cannot bind {specifier}")
        return result
