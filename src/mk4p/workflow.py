import abc
import collections
import logging
import os.path
from dataclasses import dataclass, field
from pathlib import Path
from typing import (
    Any,
    Callable,
    Dict,
    Iterable,
    Iterator,
    List,
    Match,
    Optional,
    Set,
    Tuple,
    TypeVar,
    Union,
    cast,
    runtime_checkable,
)

from typing_extensions import Protocol

from .resources import (
    ExactResourceSpecifier,
    MatchInfo,
    PathResource,
    PhonyResource,
    ResourceSpecifier,
    ResourceSpecifierHelper,
    ResourceSpecifierish,
    StemMatchInfo,
)
from .utils import FileHelper, IterableHelper, Pathlikeish, StringHelper

logger = logging.getLogger(__name__)


@runtime_checkable
class RuleSpecification(Protocol):
    @property
    def target_specifiers(self) -> List[ResourceSpecifier]:
        ...

    @property
    def prerequisite_specifiers(self) -> List[ResourceSpecifier]:
        ...


@runtime_checkable
class InvocationContext(Protocol):
    @property
    def requested_targets(self) -> List[str]:
        ...

    @property
    def exact_target_specifiers(self) -> List[ExactResourceSpecifier]:
        ...

    @property
    def targets(self) -> List[str]:
        ...

    @property
    def exact_prerequisite_specifiers(self) -> List[ExactResourceSpecifier]:
        ...

    @property
    def prerequisites(self) -> List[str]:
        ...

    @property
    def stem(self) -> Optional[str]:
        ...


@dataclass(frozen=True)
class SimpleInvocationContext:
    requested_targets: List[str]
    exact_target_specifiers: List[ExactResourceSpecifier]
    exact_prerequisite_specifiers: List[ExactResourceSpecifier]
    stem: Optional[str]
    # targets: List[str] = field(init=False)
    # prerequisites: List[str] = field(init=False)

    # def __post_init__(self) -> None:
    #     self.targets = [specifier.label for specifier in self.exact_target_specifiers]
    #     self.prerequisites = [
    #         specifier.label for specifier in self.exact_prerequisite_specifiers
    #     ]

    @classmethod
    def for_rule(
        cls, rule: "Rule", reqested_targets: List[str], stem: Optional[str]
    ) -> "SimpleInvocationContext":
        exact_target_specifiers = ResourceSpecifierHelper.bind_resources(
            rule.target_specifiers, stem
        )
        exact_prerequisite_specifiers = ResourceSpecifierHelper.bind_resources(
            rule.prerequisite_specifiers, stem
        )

        ctx = cls(
            reqested_targets,
            exact_target_specifiers,
            exact_prerequisite_specifiers,
            stem,
        )

        return ctx

    @property
    def targets(self) -> List[str]:
        return [specifier.label for specifier in self.exact_target_specifiers]

    @property
    def prerequisites(self) -> List[str]:
        return [specifier.label for specifier in self.exact_prerequisite_specifiers]


@runtime_checkable
class Rule(RuleSpecification, Protocol):
    def invoke(self, ctx: InvocationContext) -> None:
        ...


class RuleABC(abc.ABC, Rule):
    @abc.abstractproperty
    def target_specifiers(self) -> List[ResourceSpecifier]:
        ...

    @abc.abstractproperty
    def prerequisite_specifiers(self) -> List[ResourceSpecifier]:
        ...

    @abc.abstractmethod
    def invoke(self, ctx: InvocationContext) -> None:
        ...


@dataclass(frozen=True)
class RuleFunctionContext(SimpleInvocationContext):
    rule: "FunctionRule"


RuleFunctionT = Callable[[RuleFunctionContext], None]


class FunctionRule(RuleABC):
    def __init__(
        self,
        target_specifiers: List[ResourceSpecifier],
        prerequisite_specifiers: List[ResourceSpecifier],
        function: RuleFunctionT,
    ) -> None:
        super().__init__()
        self.function = function
        self._prerequisite_specifiers = prerequisite_specifiers
        self._target_specifiers = target_specifiers

    @property
    def name(self) -> str:
        return self.function.__name__

    @property
    def prerequisite_specifiers(self) -> List[ResourceSpecifier]:
        return self._prerequisite_specifiers

    @property
    def target_specifiers(self) -> List[ResourceSpecifier]:
        return self._target_specifiers

    def invoke(self, ctx: InvocationContext) -> None:
        self.function(
            RuleFunctionContext(
                ctx.requested_targets,
                ctx.exact_target_specifiers,
                ctx.exact_prerequisite_specifiers,
                ctx.stem,
                self,
            )
        )

    def __repr__(self) -> str:
        return f"<{self.__module__}.{self.__class__}(target_specifiers={self.target_specifiers!r}, prerequisite_specifiers={self.prerequisite_specifiers!r}, function={self.function!r})>"


CallableAny = Callable[..., Any]
CallableT = TypeVar("CallableT", bound=CallableAny)

# Prerequisite = Union[str, "BoundRule"]


# @dataclass
# class PrerequisiteBinding:
#     name: str


class RuleHelper:
    @classmethod
    def targets_uptodate(
        cls,
        targets: Iterable[ExactResourceSpecifier],
        prerequisites: Iterable[PathResource],
    ) -> bool:
        # missing_prerequisites = False
        newest_prerequisite_mtime_ns: Optional[int] = None
        for prerequisite in prerequisites:
            mtime_ns = prerequisite.mtime_ns()
            if mtime_ns is None:
                raise RuntimeError(f"missing prerequisite {prerequisite}")
                # missing_prerequisites = True
            elif (
                newest_prerequisite_mtime_ns is None
                or mtime_ns > newest_prerequisite_mtime_ns
            ):
                newest_prerequisite_mtime_ns = mtime_ns
        # missing_targets = False
        oldest_target_mtime_ns: Optional[int] = None
        for target in targets:
            if not isinstance(target, PathResource):
                # non path resources are never up to date
                return False
            mtime_ns = target.mtime_ns()
            if mtime_ns is None:
                # missing resources are never up to date
                # missing_targets = True
                return False
            if oldest_target_mtime_ns is None or mtime_ns < oldest_target_mtime_ns:
                oldest_target_mtime_ns = mtime_ns
        if oldest_target_mtime_ns is None:
            return False
        if newest_prerequisite_mtime_ns is None:
            return True
        return oldest_target_mtime_ns > newest_prerequisite_mtime_ns


class RuleRepository:
    def __init__(self) -> None:
        self.rules: Dict[int, Rule] = {}

    def add_rule(self, rule: Rule) -> None:
        self.rules[id(rule)] = rule

    def match_label(
        self, label: str
    ) -> Optional[Tuple[Rule, ResourceSpecifier, MatchInfo]]:
        rule: Rule
        for _, rule in self.rules.items():
            for target_specifier in rule.target_specifiers:
                match_info = target_specifier.matches_label(label)
                if match_info is not None:
                    return (rule, target_specifier, match_info)
        return None

    def match_specifier(
        self, specifier: ResourceSpecifier
    ) -> Optional[Tuple[Rule, ResourceSpecifier, MatchInfo]]:
        rule: Rule
        for _, rule in self.rules.items():
            for target_specifier in rule.target_specifiers:
                match_info = target_specifier.matches(specifier)
                if match_info is not None:
                    return (rule, target_specifier, match_info)
        return None

    def invoke(
        self,
        rule: Rule,
        reqested_targets: List[str],
        stem: Optional[str],
        rule_tracker: Optional[Set[int]] = None,
    ) -> None:
        # exact_target_specifiers = ResourceSpecifierHelper.bind_resources(
        #     rule.target_specifiers, stem
        # )
        # exact_prerequisite_specifiers = ResourceSpecifierHelper.bind_resources(
        #     rule.prerequisite_specifiers, stem
        # )

        # ctx = SimpleInvocationContext(
        #     reqested_targets,
        #     exact_target_specifiers,
        #     exact_prerequisite_specifiers,
        #     stem,
        # )
        logging.debug(
            "rule = %s, reqested_targets = %s, stem = %s", rule, reqested_targets, stem
        )

        if rule_tracker is None:
            rule_tracker = set()
        ctx = SimpleInvocationContext.for_rule(rule, reqested_targets, stem)

        path_prerequisites: List[PathResource] = []
        for specifier in ctx.exact_prerequisite_specifiers:
            match = self.match_specifier(specifier)
            if match is not None:
                prerequisite_rule, matched_specifier, match_info = match
                prerequisite_stem: Optional[str] = None
                if isinstance(match_info, StemMatchInfo):
                    prerequisite_stem = match_info.stem
                self.invoke(
                    prerequisite_rule,
                    [specifier.label],
                    prerequisite_stem,
                    rule_tracker,
                )
            else:
                if not specifier.exists():
                    raise RuntimeError(
                        f"prerequisite {specifier} does not exist and no rule to make it."
                    )
                if isinstance(specifier, PathResource):
                    path_prerequisites.append(specifier)
                else:
                    raise RuntimeError("unexpected prerequisite {specifier}")

        if RuleHelper.targets_uptodate(ctx.exact_target_specifiers, path_prerequisites):
            logger.debug("Not running rule %s as it is already up to date", rule)
            return

        # IterableHelper.gte(
        #     filter(lambda item: isinstance(item, PathResource)),
        #     [for item in path_prerequisites],
        # )

        rule_id = id(rule)
        if rule_id in rule_tracker:
            raise RuntimeError("recursive invocation of rule %s", rule)
        rule_tracker.add(rule_id)
        rule.invoke(ctx)
        rule_tracker.remove(rule_id)


class Workflow:
    def __init__(self) -> None:
        self.rule_respository = RuleRepository()

    def make(self, target: str) -> None:
        match = self.rule_respository.match_label(target)
        if match is None:
            raise RuntimeError(f"no rule to make target {target}")
        rule, matched_specifier, match_info = match
        stem: Optional[str] = None
        if isinstance(match_info, StemMatchInfo):
            stem = match_info.stem

        self.rule_respository.invoke(rule, [target], stem)

    def add_rule(self, rule: Rule) -> None:
        self.rule_respository.add_rule(rule)

    def rule(
        self,
        targets: Iterable[ResourceSpecifierish],
        prerequisites: Iterable[ResourceSpecifierish],
    ) -> Callable[[CallableT], CallableT]:
        def decorator(function: CallableT) -> CallableT:
            logger.debug("function = %s", function)
            frule = FunctionRule(
                ResourceSpecifierHelper.as_specifiers(targets),
                ResourceSpecifierHelper.as_specifiers(prerequisites),
                function,
            )
            logger.debug("frule.function = %s", frule.function)
            self.add_rule(frule)
            return function

        return decorator
